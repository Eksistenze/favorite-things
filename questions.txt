Welcome to the first Git exercise!
Completing these prompts will help you get more comfortable with
Git.

1. What is your favorite color?
  It's red.  The best color.

***
  Remember to:
    - add
    - commit
    - push
  your answer before answering the next question!
***

2. What is your favorite food?
  Za.  Gotta have that Za.


3. Who is your favorite fictional character?
  Paul Atreides


4. What is your favorite animal?
  My cats Tonkotsu and Sakura.


5. What is your favorite programming language? (Hint: You can always say Python!!)
  Yeah, I am liking Python
